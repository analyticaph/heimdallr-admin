<?php include_once('head.php'); ?>


<style>

.form-control-feedback {
  
   pointer-events: auto;
  
}


.msk-set-color-tooltip + .tooltip > .tooltip-inner { 
  
     min-width:180px;
	 background-color:red;
}

.bg{
	width:100%;
	height:100%;
}



body{
	background-color:#8080;
}

</style>
<body onLoad="RegisterTeacher()">
	<img src="../uploads/bg.jpg" class="bg" />
	
	<!--Success! - Insert-->
  	<div class="modal fade" id="TeacherForm" tabindex="-1" role="dialog" aria-labelledby="TeacherForm" aria-hidden="true">
    	<div class="modal-dialog">    
        	<div class="modal-content ">
        		<div class="modal-header bg-aqua-gradient">
          			<h4>Teacher Account Registration</h4>
        		</div>
        		<div class="modal-body bgColorWhite">
        			<form role="form" action="../index.php" method="post"  enctype="multipart/form-data" id="form1">                    
                  		<div class="box-body">

                  			<div class="form-group" id="divIndexNumber"> 
                                <label for="">ID Number</label>
                                <input type="text" class="form-control" placeholder="Enter ID Number" name="index_number" id="index_number" autocomplete="off">  
                            </div>

                  			<div class="form-group" id="divFullName"> 
                                <label for="">Full Name</label>
                                <input type="text" class="form-control" placeholder="Enter full name" name="full_name" id="full_name" autocomplete="off">  
                            </div>

                            <div class="form-group" id="divIName"> 
                                <label for="">Username</label>
                                <input type="text" class="form-control" placeholder="Enter Username" name="i_name" id="i_name" autocomplete="off">  
                            </div>

                            <div class="form-group" id="divAddress"> 
                                <label for="">Address</label>
                                <input type="text" class="form-control" placeholder="Enter Address" name="address" id="address" autocomplete="off">  
                            </div>

                            <div class="form-group" id="divPhone"> 
                                <label for="">Phone</label>
                                <input type="text" class="form-control" placeholder="09xxxxxxxxx" maxlength="11" name="phone" id="phone" autocomplete="off">  
                            </div>

                            <div class="form-group" id="divGender"> 
                                <label for="">Gender</label>
                                <select name="gender" class="form-control" id="gender" >
                                    <option>Select Gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>   
                            </div>

                    		<div class="form-group" id="divEmail">
                      			<label for="">Email</label>
                      			<input type="text" class="form-control" id="email" placeholder="Enter email address" name="email" autocomplete="off">
                    		</div>

                            <div class="form-group" id="divPassword">
                      			<label for="">Password</label>
                      			<input type="password" class="form-control" id="password" placeholder="Enter password" name="password" autocomplete="off">
                    		</div>

                    		<?php
                    		function generateRandomString($length = 10) {
    						return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
							}	  // OR: generateRandomString(24)

                    		 ?>

                    		<div class="form-group" id="divTCode">
                      			<label for="">Teacher Code</label>
                      			<input type="text" class="form-control" id="t_code" name="t_code" autocomplete="off"
                      			value="<?php echo generateRandomString(); ?>" readonly>
                    		</div>
                    		

                    		<div class="form-group" id="divPhoto">
                      			<label for="">Photo</label>
                      			<br>
                      			<img id="output" style="width:130px;height:150px;" />
                                 <input type="file" name="fileToUpload" id="fileToUpload" style="margin-top:7px;"/>
                    		</div>

                  		</div><!-- /.box-body -->
                  		<div class="box-footer">
                  			<input type="hidden" name="do" value="teacher_reg" />
                    		<button type="submit" class="btn btn-info" id="btnSubmit">Register</button>
                    		<br><br>
                    		<p>
                     		Already have an account? <a href="login.php">Login now</a>
                  			</p>
                  		</div>

                	</form>
        		</div>
      		</div>      
		</div>
	</div><!--/.Modal--> 

<script>



function RegisterTeacher(){
$('#TeacherForm').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#TeacherForm').modal('show');
};

$('#fileToUpload').change(function(){
	//MSK-00098
			
	var fileSize = this.files[0].size;	
    var maxSize = 1000000;// bytes
	var ext = $('#fileToUpload').val().split('.').pop().toLowerCase();
	var imageNoError = 0;
	
	if($.inArray(ext, ['png','jpg','jpeg']) == -1){
		//MSK-00099
		output.src="../uploads/error.png";
		$("#btnSubmit").attr("disabled", true);
		$('#divPhoto').addClass('has-error');
		$('#divPhoto').addClass('has-feedback');
		$('#divPhoto').append('<span id="spanPhoto" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"                                title="The file type is not allowed" ></span>');
		
	}else{

		if(fileSize > maxSize) {
			//MSK-00100
			output.src="../uploads/error.png";
			$("#btnSubmit").attr("disabled", true);
			$('#divPhoto').addClass('has-error');
			$('#divPhoto').addClass('has-feedback');	
			$('#divPhoto').append('<span id="spanPhoto" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip" title="The file size is too large" ></span>');		
			
					
		}else{
			// MSK-00101
			output.src = URL.createObjectURL(this.files[0]);	
			$("#btnSubmit").attr("disabled", false);	
			$('#divPhoto').removeClass('has-error');
			$('#spanPhoto').remove();// MSK-00101
			
		}
	}
});

$("form").submit(function (e) {
//MSK-000098-form submit	
	var index_number = $('#index_number').val();
	var full_name = $('#full_name').val();
	var i_name = $('#i_name').val();
	var address = $('#address').val();
	var gender = $('#gender').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var t_code = $('#t_code').val();
	var photo = $('#fileToUpload').val();
	var phone = $('#phone').val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var telformat ;
	
	
	
	if(index_number == ''){
		//MSK-00102-index_number 
		$("#btnSubmit").attr("disabled", true);
		$('#divIndexNumber').addClass('has-error has-feedback');
		$('#divIndexNumber').append('<span id="spanIndexNumber" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip"   	data-toggle="tooltip"    title="The index number is required" ></span>');	
			
		$("#index_number").keydown(function(){
			//MSK-00103-index_number 
			$("#btnSubmit").attr("disabled",false);	
			$('#divIndexNumber').removeClass('has-error has-feedback');
			$('#spanIndexNumber').remove();
			
		});

	}
	
	
	if(full_name == ''){
		//MSK-00102-full_name 
		$("#btnSubmit").attr("disabled", true);
		$('#divFullName').addClass('has-error has-feedback');
		$('#divFullName').append('<span id="spanFullName" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The full name is required" ></span>');	
		
		$("#full_name").keydown(function(){
			//MSK-00103-full_name 
			$("#btnSubmit").attr("disabled", false);	
			$('#divFullName').removeClass('has-error has-feedback');
			$('#spanFullName').remove();
			
		});
		
	}
		
	if(i_name == ''){
		//MSK-00102-i_name
		$("#btnSubmit").attr("disabled", true);
		$('#divIName').addClass('has-error has-feedback');
		$('#divIName').append('<span id="spanIName" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#i_name" ).keydown(function() {
			//MSK-00103-i_name
			$("#btnSubmit").attr("disabled", false);	
			$('#divIName').removeClass('has-error has-feedback');
			$('#spanIName').remove();
			
		});
	
	}
	
	
	
	if(address == ''){
		//MSK-00102-address
		$("#btnSubmit").attr("disabled", true);
		$('#divAddress').addClass('has-error has-feedback');
		$('#divAddress').append('<span id="spanAddress" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The address is required" ></span>');	
		
		$("#address").keydown(function() {
			//MSK-00103-address
			$("#btnSubmit").attr("disabled", false);	
			$('#divAddress').removeClass('has-error has-feedback');
			$('#spanAddress').remove();
			
		});
	
	}
	
	
	
	if(gender == 'Select Gender'){
		//MSK-00102-gender
		$("#btnSubmit").attr("disabled", true);
		$('#divGender').addClass('has-error ');
		$('#divGender').addClass('has-error has-feedback');
		$('#divGender').append('<span id="spanGender" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The gender is required" ></span>');	
		
		$("#gender").change(function() {
			//MSK-00103-gender
			$("#btnSubmit").attr("disabled", false);	
			$('#divGender').removeClass('has-error');
			$('#divGender').removeClass('has-error has-feedback');
			$('#spanGender').remove();
			
		});
	
	}
	
	
	if(email == ''){
		//MSK-00102-email	
		$('#divEmail').addClass('has-error has-feedback');
		$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The email address is required" ></span>');	
		
		$("#email").keydown(function() {
			//MSK-00103-email
			$("#btnSubmit").attr("disabled", false);	
			$('#divEmail').removeClass('has-error has-feedback');
			$('#spanEmail').remove();
			
		});
			
	}else{
		if (mailformat.test(email) == false){ 
			//MSK-00108-email
			$('#divEmail').addClass('has-error has-feedback');
			$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid email address" ></span>');
			
			$("#email").keydown(function(){
				//MSK-00109-email
				var $field = $(this);// this is the value before the keypress
				var beforeVal = $field.val();
	
				setTimeout(function() {
	
					var afterVal = $field.val();// this is the value after the keypress
					
						if (mailformat.test(afterVal) == true){
							//MSK-00110-email
							$("#btnSubmit").attr("disabled", false);
							$('#divEmail').removeClass('has-error has-feedback');
							$('#spanEmail').remove();
							$('#divEmail').addClass('has-success has-feedback');
							$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-ok form-control-feedback"></span>');
							
						}else{
							//MSK-00111-email
							$("#btnSubmit").attr("disabled", true);
							$('#spanEmail').remove();
							$('#divEmail').addClass('has-error has-feedback');
							$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid email address" ></span>');
						
						}
					
				}, 0);
						
			});
			
		}else{
			
		}
			  
	}

	if(password == ''){
		//MSK-00102-password
		$("#btnSubmit").attr("disabled", true);
		$('#divPassword').addClass('has-error has-feedback');
		$('#divPassword').append('<span id="spanPassword" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#password" ).keydown(function() {
			//MSK-00103-password
			$("#btnSubmit").attr("disabled", false);	
			$('#divPassword').removeClass('has-error has-feedback');
			$('#spanPassword').remove();
			
		});
	
	}

	if(t_code == ''){
		//MSK-00102-t_code
		$("#btnSubmit").attr("disabled", true);
		$('#divTCode').addClass('has-error has-feedback');
		$('#divTCode').append('<span id="spanTCode" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#t_code" ).keydown(function() {
			//MSK-00103-t_code
			$("#btnSubmit").attr("disabled", false);	
			$('#divTCode').removeClass('has-error has-feedback');
			$('#spanTCode').remove();
			
		});
	
	}

	if(photo == ''){
		//MSK-00102-photo
		output.src="../uploads/error.png";
		
		$("#btnSubmit").attr("disabled", true);
		$('#divPhoto').addClass('has-error has-feedback ');
		$('#divPhoto').addClass('has-feedback');
		$('#divPhoto').append('<span id="spanPhoto" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The image is required" ></span>');	
		
	}
	
	
	
	if(phone == ''){
		//MSK-00102-phone
		$('#divPhone').addClass('has-error');
		$('#divPhone').addClass('has-error has-feedback');
		$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The phone number is required" ></span>');	
				
		$("#phone").keydown(function() {
			//MSK-00103-phone
			$("#btnSubmit").attr("disabled", false);	
			$('#divPhone').removeClass('has-error');
			$('#divPhone').removeClass('has-error has-feedback');
			$('#spanPhone').remove();
			
		});
		
	}else{
		if (telformat.test(phone) == false){ 
			//MSK-00104-phone
			$('#divPhone').addClass('has-error');
			$('#divPhone').addClass('has-error has-feedback');
			$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid phone number" ></span>');
			
			$("#phone").keydown(function() {
				//MSK-00105-phone
				var $field = $(this);
				var beforeVal = $field.val();// this is the value before the keypress
	
				setTimeout(function() {
	
					var afterVal = $field.val();// this is the value after the keypress
					
						if (telformat.test(afterVal) == true){
							//MSK-00106-phone
							$("#btnSubmit").attr("disabled", false);
							$('#divPhone').removeClass('has-error');
							$('#divPhone').removeClass('has-error has-feedback');
							$('#spanPhone').remove();
							$('#divPhone').addClass('has-success has-feedback');
							$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-ok form-control-feedback"></span>');
							
						}else{
							//MSK-00107-phone
							$("#btnSubmit").attr("disabled", true);
							$('#spanPhone').remove();
							$('#divPhone').addClass('has-error');
							$('#divPhone').addClass('has-error has-feedback');
							$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid email address" ></span>');
					
						}
					
				}, 0);
						
			});
			
		}else{
			
		}
		
	}
	
	
	
	if(index_number == '' || full_name == '' || i_name == '' || address == '' || gender == 'Select Gender' || email == '' || mailformat.test(email) == false || telformat.test(phone) == false ||  password == '' ||  t_code == '' ||  photo == '' || phone == '' ) {
		
		//MSK-000098- form validation failed
		$("#btnSubmit").attr("disabled", true);
		e.preventDefault();
		return false;
			
	}else{
		$("#btnSubmit").attr("disabled", false);
	}

});
</script>


<!--run insert alert using PHP & JS/jQuery  --> 
<?php
if(isset($_GET["do"])&&($_GET["do"]=="alert_from_insert")){
  
$msg=$_GET['msg'];

	if($msg==1){
		echo"
			<script>
			
			var myModal = $('#index_Duplicated');
			myModal.modal('show');			
			
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
						
			</script>
		";
	}

	if($msg==2){//MSK-00120 
		echo"
			<script>
			
			var myModal = $('#insert_Success');
			myModal.modal('show');

			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
    		}, 3000));
			
			</script>
		";
	
	}

	if($msg==3){
		echo"
			<script>
			
			var myModal = $('#connection_Problem');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
	
	if($msg==4){
		echo"
			<script>
			
			var myModal = $('#index_email_Duplicated');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
	
	if($msg==5){
		echo"
			<script>
			
			var myModal = $('#email_Duplicated');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
	
	if($msg==6){
		echo"
			<script>
			
			var myModal = $('#upload_error1');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
}
?><!--./Insert alert --> 

<!--redirect your own url when clicking browser back button -->
<script>
(function(window, location) {
history.replaceState(null, document.title, location.pathname+"#!/history");
history.pushState(null, document.title, location.pathname);

window.addEventListener("popstate", function() {
  if(location.hash === "#!/history") {
    history.replaceState(null, document.title, location.pathname);
    setTimeout(function(){
      location.replace("../index.php");//path to when click back button
    },0);
  }
}, false);
}(window, location));
</script>
</body>