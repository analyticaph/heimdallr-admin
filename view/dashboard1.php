<?php
if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:../index.php');
    exit;
}
?>
<?php include_once('head.php'); ?>
<?php include_once('header_student.php'); ?>
<?php include_once('sidebar1.php'); ?>
<?php include_once('alert.php'); ?>

<style>

.form-control-feedback {
  
   pointer-events: auto;
  
}

.set-width-tooltip + .tooltip > .tooltip-inner { 
     min-width:180px;
}


.msk-fade {  
      
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.5s;
    animation-name: animatetop;
    animation-duration: 0.5s;
	

}

.modal-content1 {
  height: auto;
  min-height: 100%;
  border-radius: 0;
  position: absolute;
  left: 25%; 
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

.cal-table{
	
	width:100%;
	padding:0;
	margin:0;	
	
}

#calendar_dates{
	
	padding:10px;
	margin-left:35px;
	width:90%;	
	
}

.tHead{
	
	height:40px;
	background-color:#ADD8E6;
	color:black;
	text-align:center;
	border:1px solid black;
	width:70px;
}

.cal-tr{
	
	height:75px;
	
}

.td_no_number{
	
	border:1px solid black;
	width:70px;
	background-color:#ADD8E6;
	padding:0;

}

.cal-number-td{
	
	border:1px solid black;
	width:70px;
	background-color:#ffff;
	color:white;
	
}


.h5{
	
	color:black;
	display: inline-block;
	width:20px;
	height:20px;	
	font-size:14px;
	font-weight:bold;
	font-family:Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", serif;
	text-align:center;
	float:right;
	padding-top:3px;
	margin-bottom:40%;
	
}
.div-event-c{
	margin-top:65%;
	height:17px;
	
}

#cal_month{
	width:20%;
	border-radius:5%;
	
	padding:0;
}
#cal_year{
	width:15%;
	border-radius:5%;
	margin-left:5px;
	padding:0;
}

#btnShow{
	
	margin-left:5px;
	
}

</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
    	<h1>
        	Dashboard
        	<small>Preview</small>
        </h1>
        <ol class="breadcrumb">
        	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Dashboard</a></li>
    	</ol>
	</section>
    
<?php
include_once('../controller/config.php');

$my_index= $_SESSION["index_number"];

$sql="SELECT * FROM student WHERE index_number='$my_index'";
$result=mysqli_query($conn,$sql);
$row=mysqli_fetch_assoc($result);
$name=$row['i_name'];

?>    
	
     <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Student</span>
<?php
include_once('../controller/config.php');

$sql1="SELECT count(id) FROM student";
$total_count1=0;

$result1=mysqli_query($conn,$sql1);
$row1=mysqli_fetch_assoc($result1);
$total_count1=$row1['count(id)'];

?>               
              <span class="info-box-number"><?php echo $total_count1; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Teacher</span>
<?php
include_once('../controller/config.php');

$sql2="SELECT count(id) FROM teacher";
$total_count2=0;

$result2=mysqli_query($conn,$sql2);
$row2=mysqli_fetch_assoc($result2);
$total_count2=$row2['count(id)'];

?> 
              <span class="info-box-number"><?php echo $total_count2; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.coll -->



       		  
       		  <div class="col-md-8" >
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Time-In</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                     <th class="col-md-1">ID</th>
                     <th class="col-md-3">Name</th>
                     <th class="col-md-4">Time-in</th>
                     <th class="col-md-4">Date</th>
                    </thead>
                    <tbody>
                      <?php
include_once('../controller/config.php');
$my_index= $_SESSION["index_number"];
$sql="SELECT * FROM time_in INNER JOIN student on time_in.RFID_No = student.index_number WHERE student.index_number='$my_index'";
$result=mysqli_query($conn,$sql);
$count = 0;


if(mysqli_num_rows($result) > 0){

	while($row=mysqli_fetch_assoc($result)){

		$id=$row['RFID_No'];
		$id2=$row['index_number'];
		if ($id == $id2 ){
			$count++;
			
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td>
											<?php echo $row['name']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['time_in']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['Date']; ?>
                                    </td>
                                </tr>
<?php } } }  ?>
                            </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>


        	  <div class="col-md-4">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">List Of Announcements</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                     <th class="col-md-1">ID</th>
                     <th class="col-md-3">Title</th>
                     <th class="col-md-4">Date Created</th>
                    </thead>
                    <tbody>
                      <?php
include_once('../controller/config.php');
$sql="SELECT * FROM events";
$result=mysqli_query($conn,$sql);
$count = 0;


if(mysqli_num_rows($result) > 0){
	while($row=mysqli_fetch_assoc($result)){
    	$count++;
		$id=$row['id'];
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td id="td1_<?php echo $row['id']; ?>">
                                    	<a href="#" onClick="showEvent(<?php echo $row["id"]; ?>)" class=""  data-id="<?php echo $row["id"]; ?>" data-toggle="modal">
											<?php echo $row['title']; ?>
                                        </a>
                                    </td>
                                    <td>
                                    	<?php echo $row['start_date_time']; ?>

                                    	 </td>
                                </tr>
<?php } } ?>
                            </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
		</div>   



		        <div class="col-md-8">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Time-Out</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                     <th class="col-md-1">ID</th>
                     <th class="col-md-3">Name</th>
                     <th class="col-md-4">Time-Out</th>
                     <th class="col-md-4">Date</th>
                    </thead>
                    <tbody>
                      <?php
include_once('../controller/config.php');
$my_index= $_SESSION["index_number"];
$sql="SELECT * FROM time_out INNER JOIN student on time_out.RFID_No = student.index_number WHERE student.index_number='$my_index'";
$result=mysqli_query($conn,$sql);
$count = 0;

if(mysqli_num_rows($result) > 0){
	while($row=mysqli_fetch_assoc($result)){
		
		$id=$row['RFID_No'];
		$id2=$row['index_number'];
		if ($id == $id2 ){
			$count++;
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td id="td1_<?php echo $row['id']; ?>">
											<?php echo $row['name']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['time_out']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['Date']; ?>
                                    </td>
                                </tr>
<?php } } }  ?>
                            </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
		
	
    <div id="cEvent">
    
		</div>

    </div> 

  <script>

function showEvent(event_id){
  
  var xhttp = new XMLHttpRequest();//MSK-00105-Ajax Start  
    xhttp.onreadystatechange = function() {
        
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('showEvent').innerHTML = this.responseText;//MSK-000132
        $('#modalviewEvent').modal('show');
      }
        
    };  
    
    xhttp.open("GET", "show_events1.php?event_id="+event_id , true);                        
    xhttp.send();//MSK-00105-Ajax End
};


function get_calendar(day_no,days){
  
  var table = document.createElement('table');
  var tr = document.createElement('tr');
  
  table.className = 'cal-table';
  
  // row for the day letters
  for(var c=0; c<=6; c++){
    
    var th = document.createElement('th');
    th.innerHTML =  ['S','M','T','W','TH','F','S'][c];
    tr.appendChild(th);
    th.className = "tHead";
    
  }
  
  table.appendChild(tr);
  
  //create 2nd row
  
  tr = document.createElement('tr');
  
  var c;
  for(c=0; c<=6; c++){
    
    if(c== day_no){
      break;
    }
    var td = document.createElement('td');
    td.innerHTML = "";
    tr.appendChild(td);
    td.className = "td_no_number";
    tr.className = 'cal-tr';
  }
  
  var count = 1;
  for(; c<=6; c++){
    
    var td = document.createElement('td');
    td.id = "td_"+count;
    td.className = 'cal-number-td';
    tr.appendChild(td);
    tr.className = 'cal-tr';
    
    var h5 = document.createElement('h5');
    h5.className = 'h5';
    td.appendChild(h5);
    h5.innerHTML = count;
    count++;
    
  }
  table.appendChild(tr);
  
  //rest of the date rows
  
  for(var r=3; r<=7; r++){
    
    tr = document.createElement('tr');
    for(var c=0; c<=6; c++){
      
      if(count > days){
        for(; c<=6; c++){
    
          var td = document.createElement('td');
          td.innerHTML = "";
          tr.appendChild(td);
          td.className = "td_no_number";
          tr.className = 'cal-tr';
        }
        table.appendChild(tr);
        return table;
      }
      
      var td = document.createElement('td');
      //td.innerHTML = count;
      td.id = "td_"+count;
      //td.style.padding = 0;
      td.className = 'cal-number-td';
      
      tr.appendChild(td);
      
      var h5 = document.createElement('h5');
      h5.className = 'h5';
      td.appendChild(h5);
      h5.innerHTML = count;
      count++;
      tr.className = 'cal-tr';
      
    }
    table.appendChild(tr);
    
    
  }
    
};  

</script>

<!--redirect your own url when clicking browser back button -->
<script>
(function(window, location) {
history.replaceState(null, document.title, location.pathname+"#!/history");
history.pushState(null, document.title, location.pathname);

window.addEventListener("popstate", function() {
  if(location.hash === "#!/history") {
    history.replaceState(null, document.title, location.pathname);
    setTimeout(function(){
      location.replace("../index.php");//path to when click back button
    },0);
  }
}, false);
}(window, location));
</script>

</div><!-- /.content-wrapper -->    