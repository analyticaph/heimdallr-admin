<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar bg-main">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
<?php
include_once('../controller/config.php');

$index=$_SESSION["index_number"];

$sql="SELECT * FROM teacher WHERE index_number='$index'";
$result=mysqli_query($conn,$sql);
$row=mysqli_fetch_assoc($result);
$name=$row['i_name'];
$image=$row['image_name'];

?>      
      <ul class="sidebar-menu">
        <li class="treeview">
          <a href="dashboard2.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         <li>
          <a href="teacher_profile2.php">
            <i class="fa fa-flag"></i> <span>My Profile</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i>
            <span>Student</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="student2.php"><i class="fa fa-circle-o"></i> Add Student</a></li>
            <li><a href="all_student2.php"><i class="fa fa-circle-o"></i> My Student</a></li> 
            <li><a href="student_in2.php"><i class="fa fa-circle-o"></i> Time-In</a></li>
            <li><a href="student_out2.php"><i class="fa fa-circle-o"></i> Time-Out</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Parents</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="parents2.php"><i class="fa fa-circle-o"></i> Add Parents</a></li>
            <li><a href="all_parents2.php"><i class="fa fa-circle-o"></i> All Parents</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar"></i>
            <span>Announcements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="my_events2.php"><i class="fa fa-circle-o"></i> My Announcements</a></li>
            <li><a href="all_events2.php"><i class="fa fa-circle-o"></i> All Announcements</a></li>
          </ul>
        </li>
      </ul>
      <div class="sidebar-img m-3 p-3 bg-lightmain">
        <img src="../dist/img/custom/sidebar-img.png" class="img-fluid w-75 mb-3" />
        <p class="text-white mb-0">© 
          <?php
            $year = date("Y"); 
            echo $year ;
          ?>
          All Right Reserved.  Heimdallr Made by Analytica</p>
      </div>
    </section>
    <!-- /.sidebar -->
  </aside>