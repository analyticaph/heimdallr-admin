<?php
if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:../index.php');
    exit;
}
?>
<?php include_once('head.php'); ?>
<?php include_once('header_teacher.php'); ?>
<?php include_once('sidebar2.php'); ?>
<?php include_once('alert.php'); ?>

<style>

body{
	overflow-y:scroll;	
}

.msk-col-md-4{
	width:28%;
}
.modal{
	overflow-y: auto;
}

.form-control-feedback {
  
   pointer-events: auto;
  
}

.msk-set-width-tooltip + .tooltip > .tooltip-inner { 
  
     min-width:180px;
}
.msk-set-color-tooltip + .tooltip > .tooltip-inner { 
  
     min-width:180px;
	 background-color:red;
}
.msk-image-error{
	border:1px solid #f44336;
	
}

.msk-fade {  
      
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s

}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}


.modal-dialog1 {
  width: 75%;
  height: 100%;
  margin: 0;
  padding: 0;
}

.modal-content1 {
  height: auto;
  min-height: 100%;
  border-radius: 0;
  position: absolute;
  left: 16%; 
}
.modal-content2 {
  height: auto;
  min-height: 100%;
  border-radius: 0;
  position: absolute;
  left: 10%; 
}

.modal-content3 {
  height: auto;
  min-height: 100%;
  border-radius: 0;
  position: absolute;
  left: -31%; 
}

@media only screen and (max-width: 500px) {
	
	.modal-content1, .modal-content2, .modal-content3, .container, .modal-dialog1{
		
	 	width:100%;
	  	position: static;
		
		
	}
	
	#salaryDetails, #salary_details, #salary_details2{
		width:100%;
	}
	
	 #tableSdetails, #tableSdetails1, #tableSdetails2 , #tableSdetails3{
		
		width:100%;
		
	}
	
	.panel-body, .table1-responsive, .table2-responsive {
		overflow-x:auto !important; 
	}
	
	

}

@media only screen and (max-width: 768px) {
    /* For mobile phones: */
    [class*="col-"] {
        width: 100%;
    }
	
	.panel-body, .table1-responsive, .table2-responsive  {
		overflow-x:auto !important; 
	}
	
	
	.modal-content1, .modal-content2, .modal-content3, .container, .modal-dialog1{
		
	 	width:100%;
	  	position: static;
		
		
	}
	
	#salaryDetails, #salary_details, #salary_details2{
		width:100%;
	}
	
	 #tableSdetails, #tableSdetails1, tableSdetails2{
		
		width:100%;
		
	}
	
	
	
}

@media only screen and (max-width: 1200px) {
    /* For mobile phones: */
 
	
	.modal-content1, .modal-content2, .modal-content3, .container, .modal-dialog1{
		
	 	width:100%;
	  	position: static;
		
		
	}
	
	[class*="col-"] {
        width: 100%;
    }
	
	#salaryDetails, #salary_details, #salary_details2{
		width:100%;
	}
	
	 #tableSdetails, #tableSdetails1, tableSdetails2{
		
		width:100%;
		
	}
	
	.panel-body, .table1-responsive, .table2-responsive  {
		overflow-x:auto !important; 
	}
	
	
}


/* #modalINV css  */
#modalINV .div-logo {
	float: left;
	height: 130px;
}

#modalINV .logo{
	float: left;
	width: 90px;
	height: 90px;
	margin-right: 10px;
	border-radius: 50%;
	text-align: center;
	background-color:#8860a7;
}

#modalINV .class-name{
	float: left;		
	margin-top:0;
	padding-top:0;			
}

#modalINV h1,#modalINV h2,#modalINV h3{
	margin-top:0;
	color:#8860a7;

}

#modalINV .class-address {
	float: left;
			
}

#modalINV .class-email {
	float: right;
	margin-right:15px;
	padding-right:0;
	color:white;
	background-color:#8860a7;
}

#modalINV th{			
	background-color:#8860a7;
	color:white;
}
#modalINV .std-name{
	color:#8860a7;
	font-size:16px;
}
#modalINV #h1{
display:none;	
}
#modalINV .pdetail1 {
	padding:0;
	float:right;
	margin-right:7px; 
	
}

#modalINV .pdetail2 {
	float: right;
	
}

@media print{
	
	#show_INV{
		height:450px;
	}
	

	body{
		visibility: hidden;
	}
	
	#modalINV{
	   visibility: visible;
	}
	
	#tSalaryhistory{
		display:none !important;
	}

	#divPhoto{
		display:none;	
	}
	
	#modalINV .logo{
		background-color:#8860a7 !important;	
	}

	#modalINV h1,#modalINV h2,#modalINV h3,#modalINV .std-name{
		color:#8860a7 !important;	
	}
		
		
	#modalINV .table-bordered th{
		color:white!important;
		background-color:#8860a7 !important;		
	}
	#modalINV .class-email {
		color:white!important;
		background-color:#8860a7 !important;
	} 
	
	#modalINV .panel{
		border:hidden!important;
	}
	#modalINV #btn1,#modalINV .panel-footer ,#modalINV .msk-heading {
		display:none;
	}
		
	#modalINV #h1{
		display:inline;	
	}
	
	#modalINV .close{
		display:none;	
	}
	
	#modalINV .pdetail1 {
		margin:0;	
	}

	@-moz-document url-prefix() {
			
		.panel{
			margin:0;
			padding:0;
		}
		#modalINV{
			margin:0!important;
			padding:0!important;
			position:absolute;
			left:-150px;
		}
		@page{
			margin:0;
			padding:0;	
		}
	}
}


/* #modalINV1 css  */

#modalINV1 .div-logo {
	float: left;
	height: 130px;
}

#modalINV1 .logo{
	float: left;
	width: 90px;
	height: 90px;
	margin-right: 10px;
	border-radius: 50%;
	text-align: center;
	background-color:#8860a7;
}

#modalINV1 .class-name{
	float: left;		
	margin-top:0;
	padding-top:0;			
}

#modalINV1 h1,#modalINV1 h2,#modalINV1 h3{
	margin-top:0;
	color:#8860a7;

}

#modalINV1 .class-address {
	float: left;
			
}

#modalINV1 .class-email {
	float: right;
	margin-right:15px;
	padding-right:0;
	color:white;
	background-color:#8860a7;
}

#modalINV1 th{			
	background-color:#8860a7;
	color:white;
}
#modalINV1 .std-name{
	color:#8860a7;
	font-size:16px;
}
#modalINV1 #h1{
display:none;	
}

#modalINV1 .pdetail1 {
	padding:0;
	float:right;
	margin-right:7px; 
	
}

#modalINV1 .pdetail2 {
	float: right;
	
}


@media print{
	
	#show_INV{
		height:450px;
	}
	

	body{
		visibility: hidden;
	}
	
	#modalINV1{
	   visibility: visible;
	}
	
	#tSalaryhistory{
		display:none !important;
	}

	#divPhoto{
		display:none;	
	}
	
	#modalINV1 .logo{
		background-color:#8860a7 !important;	
	}

	#modalINV1 h1,#modalINV1 h2,#modalINV1 h3,#modalINV1 .std-name{
		color:#8860a7 !important;	
	}
		
		
	#modalINV1 .table-bordered th{
		color:white!important;
		background-color:#8860a7 !important;		
	}
	#modalINV1 .class-email {
		color:white!important;
		background-color:#8860a7 !important;
	} 
	
	#modalINV1 .panel{
		border:hidden!important;
	}
	#modalINV1 #btn1,#modalINV1 .panel-footer ,#modalINV1 .msk-heading {
		display:none;
	}
		
	#modalINV1 #h1{
		display:inline;	
	}
	
	#modalINV1 .close{
		display:none;	
	}
	#modalINV1 .pdetail1 {
		margin:0;	
		
	}
	@-moz-document url-prefix() {
			
		.panel{
			margin:0;
			padding:0;
		}
		#modalINV1{
			margin:0!important;
			padding:0!important;
			position:absolute;
			left:-150px;
		}
		@page{
			margin:0;
			padding:0;	
		}
	}
}


</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
    	<?php 
    	include_once('../controller/config.php');
		$sql1="SELECT * FROM teacher";
		$result1=mysqli_query($conn,$sql1);
		$row1=mysqli_fetch_assoc($result1);
		$t_code = $row1['t_code'];
    	?>
    	<h1>
        	Time-In
            <small id="t_code"><?php echo $t_code ?> </small><small>(Teacher Code)</small>
        </h1>
        <ol class="breadcrumb">
        	<li><a href="dashboard2.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-teacher"></i> Student</a></li>
            <li><a href="student_in2.php"> Student Time In</a></li>
         </ol>
     </section>

<form method='post' action='csv_in2.php'>
  
    <!-- table for view all records //MSK-00112 -->
    <section class="content" > <!-- Start of table section -->
        <div class="row" id="table1">
            <div class="col-md-8">
                <div class="box">

                    <div class="box-header">
                        <h3 class="box-title">All Student</h3> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type='submit' value='Export' name='Export'>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <th class="col-md-1">ID</th>
                                <th class="col-md-3">Name</th>
                                <th class="col-md-4">Time-in</th>
                     			<th class="col-md-4">Date</th>
                            </thead>
                            <tbody>
<?php
include_once('../controller/config.php');
$my_index1= $_SESSION["index_number"];
$sql="SELECT * FROM time_in ti INNER JOIN teacher t on ti.t_code = t.t_code WHERE t.index_number='$my_index1'";
$user_arr = array();
$result=mysqli_query($conn,$sql);
$count = 0;


if(mysqli_num_rows($result) > 0){
	while($row=mysqli_fetch_assoc($result)){
		$id=$row['t_code'];
		$id2=$row['t_code'];

	  $idd = $row['id'];
      $name = $row['name'];
      $time = $row['time_in'];
      $rfid = $row['RFID_No'];
      $status = $row['Status'];
      $date = $row['Date'];
      $t_code = $row['t_code'];
      $user_arr[] = array($id,$name,$time,$rfid,$status,$date,$t_code);

		if ($id == $id2 ){
			$count++;
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td>
											<?php echo $row['name']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['time_in']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['Date']; ?>
                                    </td>
                                </tr>
<?php } } } ?>
                            </tbody>
                        </table>
                        <?php 
    $serialize_user_arr = serialize($user_arr);
   ?>
  <textarea name='export_data' style='display: none;'><?php echo $serialize_user_arr; ?></textarea>
                	</div><!-- /.box-body -->	
                </div>
            </div>
        </div>
    </section> <!-- End of table section --> 
</form>
    

<!--run update alert using PHP & JS/JQUERY  -->    
<script>
//MSK-000125
function cTablePage(page){
	
	var currentPage1 = (page-1)*10;
	
	$(function(){
		$("#example1").DataTable({
			"displayStart": currentPage1,    
			"bDestroy": true       
   		});
						
	});
					  
	window.scrollTo(0,document.body.scrollHeight);
	
};

</script> 

<?php
// MSK-000143-24-PHP-JS-UPDATE
if(isset($_GET["do"])&&($_GET["do"]=="alert_from_update")){
  
$msg=$_GET['msg'];
$page=$_GET['page'];

	if($msg==1){
		
		echo '<script>','cTablePage('.$page.');','</script>';
		echo"
				<script>
				
				var myModal = $('#update_Success');
				myModal.modal('show');
				
				myModal.data('hideInterval', setTimeout(function(){
					myModal.modal('hide');
				}, 3000));
							
				</script>
			";
		 
	}
	
	if($msg==2){
		
		echo '<script>','cTablePage('.$page.');','</script>';
		echo"
				<script>
				
				var myModal = $('#connection_Problem');
				myModal.modal('show');
				
				myModal.data('hideInterval', setTimeout(function(){
					myModal.modal('hide');
				}, 3000));
							
				</script>
			";
		 
	}
	
	if($msg==3){
		
		echo '<script>','cTablePage('.$page.');','</script>';
		echo"
				<script>
				
				var myModal = $('#upload_error1');
				myModal.modal('show');
				
				myModal.data('hideInterval', setTimeout(function(){
					myModal.modal('hide');
				}, 3000));
							
				</script>
			";
		
	}
	
	if($msg==4){
		
		echo '<script>','cTablePage('.$page.');','</script>';
		echo"
				<script>
				
				var myModal = $('#update_error1');
				myModal.modal('show');
				
				myModal.data('hideInterval', setTimeout(function(){
					myModal.modal('hide');
				}, 3000));
							
				</script>
			";
		 
	}
	
	if($msg==5){
		
		echo '<script>','cTablePage('.$page.');','</script>';
		echo"
				<script>
				
				var myModal = $('#email_Duplicated');
				myModal.modal('show');
				
				myModal.data('hideInterval', setTimeout(function(){
					myModal.modal('hide');
				}, 3000));
							
				</script>
			";
		 
	}
	
}
?>


<!-- //MSK-00148 modalviewform-->
   
<div class="modal msk-fade" id="modalViewform" tabindex="-1" role="dialog" aria-labelledby="insert_alert1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog"><!--modal-dialog -->  
			<div class="container col-lg-12 "><!--modal-content --> 
      			<div class="row">
                    <div class="panel panel-info"><!--panel --> 
                        <div class="panel-heading">
                        	 <button type="button"  class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h3 class="panel-title" id="hname"></h3>
                        </div>
            		<div class="panel-body"><!--panel-body -->
              			<div class="row">
                			<div class="col-md-3"> 
                				<img id="photo2" alt="User Pic" src="" class="img-circle img-responsive"> 
                			</div>
                			<div class=" col-md-9"> 
                  				<table class="table table-user-information">
                    				<tbody>
                      					<tr>
                        					<td>Full Name</td>
                        					<td id="full_name2"> </td>
                      					</tr>
                      					<tr>
                        					<td>Username</td>
                        					<td id="i_name2"> </td>
                      					</tr>
                             			<tr>
                        					<td>Address</td>
                        					<td id="address2"> </td>
                      					</tr>
                        				<tr>
                        					<td>Gender</td>
                        					<td id="gender2"> </td>
                      					</tr>
                      					<tr>
                        					<td>Email</td>
                        					<td id="email2"> </td>
                      					</tr>
                        					<td>Phone Number</td>
                        					<td id="phone2"> </td>
                           
                      					</tr>
                    				</tbody>
                  				</table>
                  			</div>
                   		</div>
                  	</div><!--/.panel-body -->
            	</div><!--/. panel--> 
			</div><!--/.row --> 
    	</div><!--/.modal-content -->
	</div><!--/.modal-dialog -->
</div><!--/.modal -->  


<script>
function showModal1(Viewform){	
//MSK-00147
	var Id = $(Viewform).data("id"); 
	var path = "../"; 
	 
	var xhttp = new XMLHttpRequest();//MSK-00149-Start Ajax  
  		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				//MSK-00150
				var myArray1 = eval( xhttp.responseText );
				
				document.getElementById("full_name2").innerHTML =myArray1[1];
				document.getElementById("i_name2").innerHTML =myArray1[2];
				document.getElementById("hname").innerHTML =myArray1[2];
				document.getElementById("address2").innerHTML =myArray1[3];
				document.getElementById("gender2").innerHTML =myArray1[4];
				document.getElementById("phone2").innerHTML =myArray1[5];
				document.getElementById("email2").innerHTML =myArray1[6];
				document.getElementById("photo2").src ="../"+myArray1[7];
			
			
			}
			
  		};	
		
    xhttp.open("GET", "../model/get_student.php?id=" + Id  , true);												
  	xhttp.send();//MSK-00149--End Ajax
	 
};



function scrollDown(){
	
	window.scrollTo(0,document.body.scrollHeight);
}

</script>


<!--redirect your own url when clicking browser back button -->
<script>
(function(window, location) {
history.replaceState(null, document.title, location.pathname+"#!/history");
history.pushState(null, document.title, location.pathname);

window.addEventListener("popstate", function() {
  if(location.hash === "#!/history") {
    history.replaceState(null, document.title, location.pathname);
    setTimeout(function(){
      location.replace("../index.php");//path to when click back button
    },0);
  }
}, false);
}(window, location));
</script>
     
</div><!-- /.content-wrapper -->  