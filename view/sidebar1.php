<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar bg-main">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
<?php
include_once('../controller/config.php');

$index=$_SESSION["index_number"];

$sql="SELECT * FROM student WHERE index_number='$index'";
$result=mysqli_query($conn,$sql);
$row=mysqli_fetch_assoc($result);
$name=$row['i_name'];
$image=$row['image_name'];

?>      
      <ul class="sidebar-menu">
        <li class="treeview">
          <a href="dashboard1.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="my_profile.php">
            <i class="fa fa-flag"></i> <span>My Profile</span>
          </a>
        </li>
        <li>
          <a  href="all_events3.php">
            <i class="fa fa-calendar-check-o"></i> <span>All Announcements</span>
          </a>
        </li>
      </ul>
      <div class="sidebar-img m-3 p-3 bg-lightmain">
        <img src="../dist/img/custom/sidebar-img.png" class="img-fluid w-75 mb-3" />
        <p class="text-white mb-0">© 
          <?php
            $year = date("Y"); 
            echo $year ;
          ?>
          All Right Reserved.  Heimdallr Made by Analytica</p>
      </div>
    </section>
    <!-- /.sidebar -->
  </aside>