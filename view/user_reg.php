<?php include_once('head.php'); ?>


<style>

.form-control-feedback {
  
   pointer-events: auto;
  
}


.msk-set-color-tooltip + .tooltip > .tooltip-inner { 
  
     min-width:180px;
	 background-color:red;
}

</style>
<body onLoad="Register()">
	<div class="custom-login">
		<div class="osition-relative row m-0 h-100">
			<div class="position-fixed col-md-6 reg-design zcol-md-6 p-5 bg-main text-center justify-content-center align-items-center flex-column d-none d-md-flex">
				<h2 class="text-white">Nice to see you again</h2>
				<h1 class="font-weight-bold display-4 text-white mb-5">Welcome Back!</h1>
				<img src="../dist/img/custom/login-img.png" class="img-fluid" />
			</div>
			<div class="position-absolute col-md-6 reg-form zcol-md-6 d-flex justify-content-center align-items-center flex-column">
				<div class="m-5">
					<div class="text-center mb-5">
						<img src="../dist/img/custom/main-logo.png" class="img-fluid w-50 mb-3" />
						<p class="mb-4 lead font-weight-bold">Student Account Registration</p>
					</div>
					<form role="form" action="../index.php" method="post"  enctype="multipart/form-data" id="form1">                    
						<div class="form-row">
							<div class="form-group col-md-12" id="divFullName"> 
								<label for=""  class="font-weight-bold">Full Name</label>
								<input type="text" class="form-control" placeholder="Enter full name" name="full_name" id="full_name" autocomplete="off">  
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6" id="divPhone"> 
								<label for=""  class="font-weight-bold">School Number</label>
								<input type="text" class="form-control" id="school_id" placeholder="Enter School Id Number" name="school_id" autocomplete="off">
							</div>
							<div class="form-group col-md-6" id="divGender"> 
								<label for=""  class="font-weight-bold">Code Number</label>
								<input type="text" class="form-control" id="t_code" placeholder="Enter Teacher Provided Code Number" name="t_code" autocomplete="off">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6" id="divIName"> 
								<label for=""  class="font-weight-bold">Username</label>
								<input type="text" class="form-control" placeholder="Enter Username" name="i_name" id="i_name" autocomplete="off">  
							</div>
							<div class="form-group col-md-6" id="divPhone">
									<label for=""  class="font-weight-bold">Phone</label>
									<input type="text" class="form-control" placeholder="09xxxxxxxxx" maxlength="11" name="phone" id="phone" autocomplete="off">  
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6" id="divGender">
								<label for=""  class="font-weight-bold">Gender</label>
								<select name="gender" class="form-control" id="gender" >
                                    <option>Select Gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>   
							</div>
							<div class="form-group col-md-6" id="divDOB">
								<label for=""  class="font-weight-bold">Date Of Birth</label>
								<input type="date" class="form-control" name="b_date" id="b_date" autocomplete="off">  
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6" id="divEmail">
								<label for=""  class="font-weight-bold">Email</label>
								<input type="text" class="form-control" id="email" placeholder="Enter email address" name="email" autocomplete="off">
							</div>
							<div class="form-group col-md-6" id="divPassword">
								<label for=""  class="font-weight-bold">Password</label>
								<input type="password" class="form-control" id="password" placeholder="Enter password" name="password" autocomplete="off">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-12" id="divAddress"> 
								<label for=""  class="font-weight-bold">Address</label>
								<input type="text" class="form-control" placeholder="Enter Address" name="address" id="address" autocomplete="off">  
							</div>
						</div>
						<div class="form-row flex-md-row-reverse">
							<div class="form-group col-md-6" id="divPhoto">
								<label for=""  class="font-weight-bold">Photo</label>
								<br>
								<div class="border-main position-relative" style="height:250px; width:250px">
									<div class="h-100 w-100 p-3">
										<div class="image-container rounded-lg  d-flex align-items-center justify-content-center">
											<div class="p-4 text-center position-absolute message d-flex flex-column align-items-center" id="uploadMessage">
												<h1 class="text-main mb-0"><i class="fa fa-cloud-upload" aria-hidden="true"></i></h1>
												<span id="uploadError">Click here to upload image</span>
											</div>
											<img id="output" class="img-upload w-100" />
											<div  class="text-break hide font-weight-bold mb-0 w-100 bg-dark text-white px-2 py-1 text-center" id="fileToUploadName"></div>
										</div>
										<input type="file" class="w-100 position-absolute" name="fileToUpload" id="fileToUpload"/>
									</div>
								</div>
								<div  class="mt-2" id="errorMessage"></div>
							</div>
							<div class="form-group col-md-6 mt-auto">
								<input type="hidden" name="do" value="user_reg" />
									<button type="submit" class="btn bg-sub text-white px-5" id="btnSubmit">Sign Up</button>
									<br><br>
									<p>
									Already have an account?<br /><a href="login.php">Login now</a>
									</p>
							</div>
						</div>
          </form>
				</div>
			</div>
		</div>
	</div>

<script>

function Register(){
$('#RegisterForm').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#RegisterForm').modal('show');
};

$('#fileToUpload').change(function(){
	//MSK-00098
			
	var fileSize = this.files[0].size;	
    var maxSize = 1000000;// bytes
	var ext = $('#fileToUpload').val().split('.').pop().toLowerCase();
	var fileName = this.files[0].name;
	var imageNoError = 0;
	
	$('#uploadMessage').addClass('hide');
	$('#fileToUploadName').removeClass('hide');
	$('#fileToUploadName').text(fileName);

	if($.inArray(ext, ['png','jpg','jpeg']) == -1){
		//MSK-00099
		output.src="../uploads/error-picture.png";
		$("#btnSubmit").attr("disabled", true);
		$('#divPhoto').addClass('has-error');
		$('#divPhoto').addClass('has-feedback');
		$('#errorMessage').text('Please upload following image type: .png, .jpg, .jpeg');
		$('#divPhoto').append('<span id="spanPhoto" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"                                title="The file type is not allowed" ></span>');
		
	}else{
		$('#errorMessage').text('');
		if(fileSize > maxSize) {
			//MSK-00100
			output.src="../uploads/error-picture.png";
			$('#errorMessage').text('Oops! The size limit for the image is 1.0 MB. Reduce the image size and try again.');
			$("#btnSubmit").attr("disabled", true);
			$('#divPhoto').addClass('has-error');
			$('#divPhoto').addClass('has-feedback');	
			$('#divPhoto').append('<span id="spanPhoto" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip" title="The file size is too large" ></span>');		
			
					
		}else{
			// MSK-00101
			output.src = URL.createObjectURL(this.files[0]);	
			$("#btnSubmit").attr("disabled", false);	
			$('#divPhoto').removeClass('has-error');
			$('#spanPhoto').remove();// MSK-00101
			
		}
	}
});

$("form").submit(function (e) {
//MSK-000098-form submit	
	var full_name = $('#full_name').val();
	var i_name = $('#i_name').val();
	var address = $('#address').val();
	var gender = $('#gender').val();
	var b_date = $('#b_date').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var t_code = $('#t_code').val();
	var school_id = $('#school_id').val();
	var photo = $('#fileToUpload').val();
	var phone = $('#phone').val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var telformat ;
	
	
	
	
	
	
	if(full_name == ''){
		//MSK-00102-full_name 
		$("#btnSubmit").attr("disabled", true);
		$('#divFullName').addClass('has-error has-feedback');
		$('#divFullName').append('<span id="spanFullName" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The full name is required" ></span>');	
		
		$("#full_name").keydown(function(){
			//MSK-00103-full_name 
			$("#btnSubmit").attr("disabled", false);	
			$('#divFullName').removeClass('has-error has-feedback');
			$('#spanFullName').remove();
			
		});
		
	}
		
	if(i_name == ''){
		//MSK-00102-i_name
		$("#btnSubmit").attr("disabled", true);
		$('#divIName').addClass('has-error has-feedback');
		$('#divIName').append('<span id="spanIName" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#i_name" ).keydown(function() {
			//MSK-00103-i_name
			$("#btnSubmit").attr("disabled", false);	
			$('#divIName').removeClass('has-error has-feedback');
			$('#spanIName').remove();
			
		});
	
	}
	
	
	
	if(address == ''){
		//MSK-00102-address
		$("#btnSubmit").attr("disabled", true);
		$('#divAddress').addClass('has-error has-feedback');
		$('#divAddress').append('<span id="spanAddress" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The address is required" ></span>');	
		
		$("#address").keydown(function() {
			//MSK-00103-address
			$("#btnSubmit").attr("disabled", false);	
			$('#divAddress').removeClass('has-error has-feedback');
			$('#spanAddress').remove();
			
		});
	
	}
	
	
	
	if(gender == 'Select Gender'){
		//MSK-00102-gender
		$("#btnSubmit").attr("disabled", true);
		$('#divGender').addClass('has-error ');
		$('#divGender').addClass('has-error has-feedback');
		$('#divGender').append('<span id="spanGender" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The gender is required" ></span>');	
		
		$("#gender").change(function() {
			//MSK-00103-gender
			$("#btnSubmit").attr("disabled", false);	
			$('#divGender').removeClass('has-error');
			$('#divGender').removeClass('has-error has-feedback');
			$('#spanGender').remove();
			
		});
	
	}
	
	
	
	if(b_date == ''){
		//MSK-00102-address
		$("#btnSubmit").attr("disabled", true);
		$('#divDOB').addClass('has-error');
		$('#divDOB').addClass('has-error has-feedback');
		$('#divDOB').append('<span id="spanDOB" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The date of birth is required" ></span>');	
		
		$("#b_date").keydown(function() {
			//MSK-00103-address
			$("#btnSubmit").attr("disabled", false);	
			$('#divDOB').removeClass('has-error');
			$('#divDOB').removeClass('has-error has-feedback');
			$('#spanDOB').remove();
			
		});
	
	}
	
	
	
	if(email == ''){
		//MSK-00102-email	
		$('#divEmail').addClass('has-error has-feedback');
		$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The email address is required" ></span>');	
		
		$("#email").keydown(function() {
			//MSK-00103-email
			$("#btnSubmit").attr("disabled", false);	
			$('#divEmail').removeClass('has-error has-feedback');
			$('#spanEmail').remove();
			
		});
			
	}else{
		if (mailformat.test(email) == false){ 
			//MSK-00108-email
			$('#divEmail').addClass('has-error has-feedback');
			$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid email address" ></span>');
			
			$("#email").keydown(function(){
				//MSK-00109-email
				var $field = $(this);// this is the value before the keypress
				var beforeVal = $field.val();
	
				setTimeout(function() {
	
					var afterVal = $field.val();// this is the value after the keypress
					
						if (mailformat.test(afterVal) == true){
							//MSK-00110-email
							$("#btnSubmit").attr("disabled", false);
							$('#divEmail').removeClass('has-error has-feedback');
							$('#spanEmail').remove();
							$('#divEmail').addClass('has-success has-feedback');
							$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-ok form-control-feedback"></span>');
							
						}else{
							//MSK-00111-email
							$("#btnSubmit").attr("disabled", true);
							$('#spanEmail').remove();
							$('#divEmail').addClass('has-error has-feedback');
							$('#divEmail').append('<span id="spanEmail" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid email address" ></span>');
						
						}
					
				}, 0);
						
			});
			
		}else{
			
		}
			  
	}

	if(password == ''){
		//MSK-00102-password
		$("#btnSubmit").attr("disabled", true);
		$('#divPassword').addClass('has-error has-feedback');
		$('#divPassword').append('<span id="spanPassword" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#password" ).keydown(function() {
			//MSK-00103-password
			$("#btnSubmit").attr("disabled", false);	
			$('#divPassword').removeClass('has-error has-feedback');
			$('#spanPassword').remove();
			
		});
	
	}

	if(t_code == ''){
		//MSK-00102-t_code
		$("#btnSubmit").attr("disabled", true);
		$('#divTCode').addClass('has-error has-feedback');
		$('#divTCode').append('<span id="spanTCode" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#t_code" ).keydown(function() {
			//MSK-00103-t_code
			$("#btnSubmit").attr("disabled", false);	
			$('#divTCode').removeClass('has-error has-feedback');
			$('#spanTCode').remove();
			
		});
	
	}

	if(school_id == ''){
		//MSK-00102-t_code
		$("#btnSubmit").attr("disabled", true);
		$('#divSchool').addClass('has-error has-feedback');
		$('#divSchool').append('<span id="spanSchool" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The initials name is required" ></span>');	
		
		$( "#t_code" ).keydown(function() {
			//MSK-00103-t_code
			$("#btnSubmit").attr("disabled", false);	
			$('#divSchool').removeClass('has-error has-feedback');
			$('#spanSchool').remove();
			
		});
	
	}

	if(photo == ''){
		//MSK-00102-photo
		output.src="../uploads/error-picture.png";
		$('#uploadMessage').addClass('hide');
		$('#errorMessage').text("Please upload student's profile picture.");
		$("#btnSubmit").attr("disabled", true);
		$('#divPhoto').addClass('has-error has-feedback ');
		$('#divPhoto').addClass('has-feedback');
		$('#divPhoto').append('<span id="spanPhoto" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The image is required" ></span>');	
		
	}
	
	
	
	if(phone == ''){
		//MSK-00102-phone
		$('#divPhone').addClass('has-error');
		$('#divPhone').addClass('has-error has-feedback');
		$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-remove form-control-feedback msk-set-width-tooltip" data-toggle="tooltip"    title="The phone number is required" ></span>');	
				
		$("#phone").keydown(function() {
			//MSK-00103-phone
			$("#btnSubmit").attr("disabled", false);	
			$('#divPhone').removeClass('has-error');
			$('#divPhone').removeClass('has-error has-feedback');
			$('#spanPhone').remove();
			
		});
		
	}else{
		if (telformat.test(phone) == false){ 
			//MSK-00104-phone
			$('#divPhone').addClass('has-error');
			$('#divPhone').addClass('has-error has-feedback');
			$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid phone number" ></span>');
			
			$("#phone").keydown(function() {
				//MSK-00105-phone
				var $field = $(this);
				var beforeVal = $field.val();// this is the value before the keypress
	
				setTimeout(function() {
	
					var afterVal = $field.val();// this is the value after the keypress
					
						if (telformat.test(afterVal) == true){
							//MSK-00106-phone
							$("#btnSubmit").attr("disabled", false);
							$('#divPhone').removeClass('has-error');
							$('#divPhone').removeClass('has-error has-feedback');
							$('#spanPhone').remove();
							$('#divPhone').addClass('has-success has-feedback');
							$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-ok form-control-feedback"></span>');
							
						}else{
							//MSK-00107-phone
							$("#btnSubmit").attr("disabled", true);
							$('#spanPhone').remove();
							$('#divPhone').addClass('has-error');
							$('#divPhone').addClass('has-error has-feedback');
							$('#divPhone').append('<span id="spanPhone" class="glyphicon glyphicon-remove form-control-feedback msk-set-color-tooltip" data-toggle="tooltip"    title="Enter valid email address" ></span>');
					
						}
					
				}, 0);
						
			});
			
		}else{
			
		}
		
	}
	
	
	
	if(full_name == '' || i_name == '' || address == '' || gender == 'Select Gender' || b_date == '' || email == '' || mailformat.test(email) == false || telformat.test(phone) == false ||  password == '' ||  t_code == '' ||  school_id == '' ||  photo == '' || phone == '' ) {
		
		//MSK-000098- form validation failed
		$("#btnSubmit").attr("disabled", true);
		e.preventDefault();
		return false;
			
	}else{
		$("#btnSubmit").attr("disabled", false);
	}

});
</script>


<!--run insert alert using PHP & JS/jQuery  --> 
<?php
if(isset($_GET["do"])&&($_GET["do"]=="alert_from_insert")){
  
$msg=$_GET['msg'];

	if($msg==1){
		echo"
			<script>
			
			var myModal = $('#index_Duplicated');
			myModal.modal('show');			
			
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
						
			</script>
		";
	}

	if($msg==2){//MSK-00120 
		echo"
			<script>
			
			var myModal = $('#insert_Success');
			myModal.modal('show');

			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
    		}, 3000));
			
			</script>
		";
	
	}

	if($msg==3){
		echo"
			<script>
			
			var myModal = $('#connection_Problem');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
	
	if($msg==4){
		echo"
			<script>
			
			var myModal = $('#index_email_Duplicated');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
	
	if($msg==5){
		echo"
			<script>
			
			var myModal = $('#email_Duplicated');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
	
	if($msg==6){
		echo"
			<script>
			
			var myModal = $('#upload_error1');
			myModal.modal('show');
			
			clearTimeout(myModal.data('hideInterval'));
    		myModal.data('hideInterval', setTimeout(function(){
    			myModal.modal('hide');
				
    		}, 3000));
			
			</script>
		";
	
	}
}
?><!--./Insert alert --> 

<!--redirect your own url when clicking browser back button -->
<script>
(function(window, location) {
history.replaceState(null, document.title, location.pathname+"#!/history");
history.pushState(null, document.title, location.pathname);

window.addEventListener("popstate", function() {
  if(location.hash === "#!/history") {
    history.replaceState(null, document.title, location.pathname);
    setTimeout(function(){
      location.replace("../index.php");//path to when click back button
    },0);
  }
}, false);
}(window, location));
</script>
</body>