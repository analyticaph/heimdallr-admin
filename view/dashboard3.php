<?php
if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location:../index.php');
    exit;
}
?>
<?php include_once('head.php'); ?>
<?php include_once('header_parents.php'); ?>
<?php include_once('sidebar3.php'); ?>
<?php include_once('alert.php'); ?>

<style>

.form-control-feedback {
  
   pointer-events: auto;
  
}

.set-width-tooltip + .tooltip > .tooltip-inner { 
     min-width:180px;
}


.msk-fade {  
      
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.5s;
    animation-name: animatetop;
    animation-duration: 0.5s;
	

}

.modal-content1 {
  height: auto;
  min-height: 100%;
  border-radius: 0;
  position: absolute;
  left: 25%; 
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

.cal-table{
	
width:100%;
padding:0;
margin:0;	
}

#calendar_dates{
	padding:10px;
	margin-left:10px;
	width:95%;	
	
}

.tHead{
	
	height:40px;
	background-color:#ADD8E6;
	color:black;
	text-align:center;
	border:1px solid black;
	width:70px;
}

.cal-tr{
	height:50px;
	
}

.td_no_number{
	
	border:1px solid black;
	width:70px;
	background-color:#ADD8E6;
	padding:0;

}

.cal-number-td{
	
	border:1px solid black;
	width:70px;
	background-color:#ffff;
	color:white;
	
}



.h5{
	
	color:black;
	display: inline-block;
	width:20px;
	height:20px;	
	font-size:14px;
	font-weight:bold;
	font-family:Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", serif;
	text-align:center;
	float:right;
	padding-top:3px;
	margin-bottom:40%;
	
}
.div-event-c{
	margin-top:65%;
	height:17px;
	
}

#cal_month{
	width:20%;
	border-radius:5%;
	
	padding:0;
}
#cal_year{
	width:15%;
	border-radius:5%;
	margin-left:5px;
	padding:0;
}

#btnShow{
	
	margin-left:5px;
	
}

</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
    	<h1>
        	Dashboard
        	<small>Preview</small>
        </h1>
        <ol class="breadcrumb">
        	<li><a href="dashboard3.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Dashboard</a></li>
    	</ol>
	</section>
    
<?php
include_once('../controller/config.php');

$my_index= $_SESSION["index_number"];

$sql1="SELECT * FROM parents WHERE index_number='$my_index'";
$result1=mysqli_query($conn,$sql1);
$row1=mysqli_fetch_assoc($result1);
$my_son_index=$row1['my_son_index'];
$name=$row1['i_name'];

?>    
	
     <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
            	<span class="info-box-text">Total Student</span>
<?php
include_once('../controller/config.php');

$sql1="SELECT count(id) FROM student";
$total_count1=0;

$result1=mysqli_query($conn,$sql1);
$row1=mysqli_fetch_assoc($result1);
$total_count1=$row1['count(id)'];

?>               
            	<span class="info-box-number"><?php echo $total_count1; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
            	<span class="info-box-text">Total Teacher</span>
<?php
include_once('../controller/config.php');

$sql2="SELECT count(id) FROM teacher";
$total_count2=0;

$result2=mysqli_query($conn,$sql2);
$row2=mysqli_fetch_assoc($result2);
$total_count2=$row2['count(id)'];

?> 
              	<span class="info-box-number"><?php echo $total_count2; ?></span>
            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

       		        <div class="col-md-8">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Time-In</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                     <th class="col-md-1">ID</th>
                     <th class="col-md-3">Name</th>
                     <th class="col-md-4">Time-in</th>
                     <th class="col-md-4">Date</th>
                    </thead>
                    <tbody>
                      <?php
include_once('../controller/config.php');
$my_index1= $_SESSION["index_number"];
$sql="SELECT * FROM time_in t INNER JOIN parents p on t.RFID_No = p.my_son_index WHERE p.index_number='$my_index1'";
$result=mysqli_query($conn,$sql);
$count = 0;


if(mysqli_num_rows($result) > 0){
	while($row=mysqli_fetch_assoc($result)){
		$id=$row['RFID_No'];
		$id2=$row['my_son_index'];
		if ($id == $id2 ){
			$count++;
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td>
											<?php echo $row['name']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['time_in']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['Date']; ?>
                                    </td>
                                </tr>
<?php } } } ?>
                            </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
		</div>

		<div class="col-md-4">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">List Of Announcements</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                     <th class="col-md-1">ID</th>
                     <th class="col-md-3">Title</th>
                     <th class="col-md-4">Date Created</th>
                    </thead>
                    <tbody>
                      <?php
include_once('../controller/config.php');
$sql="SELECT * FROM events";
$result=mysqli_query($conn,$sql);
$count = 0;


if(mysqli_num_rows($result) > 0){
	while($row=mysqli_fetch_assoc($result)){
    	$count++;
		$id=$row['id'];
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td id="td1_<?php echo $row['id']; ?>">
                                    	<a href="#" onClick="showEvent(<?php echo $row["id"]; ?>)" class=""  data-id="<?php echo $row["id"]; ?>" data-toggle="modal">
											<?php echo $row['title']; ?>
                                        </a>
                                    </td>
                                    <td>
                                    	<?php echo $row['start_date_time']; ?>

                                    	 </td>
                                </tr>
<?php } } ?>
                            </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
		</div>   



		        <div class="col-md-8">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Time-Out</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                     <th class="col-md-1">ID</th>
                     <th class="col-md-3">Name</th>
                     <th class="col-md-4">Time-Out</th>
                     <th class="col-md-4">Date</th>
                    </thead>
                    <tbody>
                      <?php
include_once('../controller/config.php');
$my_index1= $_SESSION["index_number"];
$sql="SELECT * FROM time_out t INNER JOIN parents p on t.RFID_No = p.my_son_index WHERE p.index_number='$my_index1'";
$result=mysqli_query($conn,$sql);
$count = 0;


if(mysqli_num_rows($result) > 0){
	while($row=mysqli_fetch_assoc($result)){
    	$id=$row['RFID_No'];
		$id2=$row['my_son_index'];
		if ($id == $id2 ){
			$count++;
?>   
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td id="td1_<?php echo $row['id']; ?>">
											<?php echo $row['name']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['time_out']; ?>
                                    </td>
                                    <td>
                                    	<?php echo $row['Date']; ?>
                                    </td>
                                </tr>
<?php } } } ?>
                            </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
	
    <div id="cEvent">
    
    </div>  
    


<!--redirect your own url when clicking browser back button -->
<script>
(function(window, location) {
history.replaceState(null, document.title, location.pathname+"#!/history");
history.pushState(null, document.title, location.pathname);

window.addEventListener("popstate", function() {
  if(location.hash === "#!/history") {
    history.replaceState(null, document.title, location.pathname);
    setTimeout(function(){
      location.replace("../index.php");//path to when click back button
    },0);
  }
}, false);
}(window, location));
</script>

</div>