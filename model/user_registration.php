<?php
include_once('controller/config.php');
if(isset($_POST["do"])&&($_POST["do"]=="user_reg")){

	$full_name = $_POST["full_name"];
	$gender = $_POST["gender"];
	$address = $_POST["address"];
	$i_name = $_POST["i_name"];
	$phone = $_POST["phone"];
	$email = $_POST["email"];
	$b_date = $_POST["b_date"];
	$t_code = $_POST["t_code"];
	$password = $_POST["password"];
	$school_id = $_POST["school_id"];
	$status = "EXITS the Campus";
	
	$reg_year=date("Y");
	$reg_month=date("F");
	$reg_date=date("Y-m-d");
	
	$target_dir = "uploads/";
	$name = basename($_FILES["fileToUpload"]["name"]);
	$size = $_FILES["fileToUpload"]["size"];
	$type = $_FILES["fileToUpload"]["type"];
	$tmpname = $_FILES["fileToUpload"]["tmp_name"];
	$file = addslashes(file_get_contents($_FILES["fileToUpload"]["tmp_name"]));

	$max = 31457280;
	$extention = strtolower(substr($name, strpos($name, ".")+ 1));
	$filename = date("Ymjhis");

	$image_path =  $target_dir.$filename.".".$extention;
	
	//Insert Student--------------------------------------------------
	$msg=0;//for alerts
	
	$sql2="SELECT * FROM student where email='$email'";	
	$result2=mysqli_query($conn,$sql2);
	$row2=mysqli_fetch_assoc($result2);
	$email2=$row2['email'];

	$sql5="SELECT * FROM teacher where t_code='$t_code'";	
	$result5=mysqli_query($conn,$sql5);
	$row5=mysqli_fetch_assoc($result5);
	$t_code2=$row5['t_code'];


	if($email == $email2){
		
		//MSK-000143-3 Only email address duplicates.
		$msg+=5;
		
	}

	else{
		//MSK-000143-4
		if(($extention == "jpg" || $extention == "jpeg" || $extention == "png") && $size < $max){
			if(move_uploaded_file($tmpname, $image_path)){				
				//MSK-000143-5	

				$sql = "INSERT INTO student (full_name,i_name,gender,address,phone,email,image_name,reg_year,reg_month,reg_date,
				file_name,_status, b_date, t_code, school_id)
			            VALUES ('".$full_name."','".$i_name."','".$gender."','".$address."','".$phone."','".$email."','".$image_path."','".$reg_year."','".$reg_month."','".$reg_date."','".$file."','".$status."','".$b_date."','".$t_code."','".$school_id."')";

			    $sql1 = "INSERT INTO student_res (full_name,i_name,gender,address,phone,email,image_name,reg_year,reg_month,
			    reg_date,file_name,_status, b_date, school_id)
			            VALUES ('".$full_name."','".$i_name."','".$gender."','".$address."','".$phone."','".$email."','".$image_path."','".$reg_year."','".$reg_month."','".$reg_date."','".$file."','".$status."','".$b_date."','".$school_id."')";

			     $query = mysqli_query($conn,$sql1);
			     
				if(mysqli_query($conn,$sql)){
					$msg+=2;  
					//MSK-000143-6 The record has been successfully inserted into the database.
					$sql3= "INSERT INTO user (email,password,type)
			                VALUES ('".$email."','".$password."','Student')";
					mysqli_query($conn,$sql3);
				}else{
					$msg+=3;  
					//MSK-000143-7 Connection problem.
				}
				
			}else{
				//MSK-000143-8 If there aren't any folder - "uploads"
				$msg+=6; 
			}
		}else{
			//you can do anything when image type or size have a problem, but we have checked it before submit the form. 
			//And you need to know, how to check that using PHP
		}
	
	}
	
	header("Location: view/user_reg.php?do=alert_from_insert&msg=$msg");
	
}
?>